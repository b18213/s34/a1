/*Load the expressjs module into our application and saved it in a variable called express*/

const express = require("express");
/*
	This creates an application that uses express and store it as app
	app is our server
*/
const app = express();

const port = 4000;

// <Buffer 35c 25a 45d
// middleware 
app.use(express.json())

// mock data
let users = [
	{
		username : "BMadrigal",
		email : "fateReader@gmail.com",
		password: "donTalkAboutME"
	},
	{
		username : "Luisa",
		email : "stronggirl@gmail.com",
		password: "pressure"
	},

];

let items = [
	{
		name: "roses",
		price: 170,
		isActive: true
	},
	{
		name: "tulips",
		price: 250,
		isActive: true
	},
	
];

// app.get(<endpoint>, <function for req and res>)

app.get('/', (req, res) => {

	res.send('Hello from my first expressJS API')
	// res.status(200).send('Hello from my first expressJS API')
});



/*Mini Activity */	

app.get('/greeting', (req, res) => {

	res.send('Hello from Batch 182-Cordovae')
});

/*end of mini activity*/	

// retrieval of the mock database
app.get('/users', (req, res)=>{

	// res.send already stringifies for you
	res.send(users);
	// res.json(users);
});

app.post('/users', (req,res) => {

	console.log(req.body);
	let newUser = {
		username : req.body.username,
		email : req.body.email,
		password : req.body.password

	}

	users.push(newUser);
	console.log(users);

	res.send(users);
});

app.delete('/users', (req, res) =>{

	users.pop();
	console.log(users);

	// send the updated users array
	res.send(users);
});

// PUT method
// update user's password
//  : index - wildcard
// url : localhost:4000/users/0
app.put('/users/:index', (req, res) =>{

	console.log(req.body)

	// an object that contains the value of URL params
	console.log(req.params)

	// parseInt the value of the number coming from req.params
	let index = parseInt(req.params.index);

	// users[0].password
	users[index].password = req.body.password;

	res.send(users[index]);
});

/*
	Mini-Activity

	>> endpoint: users/:index
	>> method: PUT

	>> Update a user's username
	>> Specificy the user using the index in the params
	>> put the updated username in request body
	>> send the updated user as a response
	>> test in Postman
*/

app.put('/users/update/:index', (req, res) =>{
	
	let index = parseInt(req.params.index);

	users[index].username = req.body.username;

	res.send(users[index]);
});

// Retrieval of single user

app.get('/users/getSingleUser/:index', (req,res) =>{

	console.log(req.params)

	let index = parseInt(req.params.index)
	// parseInt('0')
	console.log(index);

	res.send(users[index]);
	console.log(users[index]);

});

// Activty Solution

app.get('/items', (req,res) => {

	res.send(items);
})

app.post('/items', (req,res) =>{

	let newItems = {
		name : req.body.name,
		price : req.body.price,
		isActive : req.body.isActive

	}

	items.push(newItems);
	res.send(items);

})

app.put('/items/:index', (req,res) =>{

	let index = parseInt(req.params.index);
	items[index].price = req.body.price

	res.send(items[index]);
})


// Activity #2

app.get('/items/getSingleItem/:index', (req,res) =>{

	let index = parseInt(req.params.index);


	res.send(items[index]);
})

app.put('/items/archive/:index', (req,res) =>{

	let index = parseInt(req.params.index);
	items[index].isActive = false;

	res.send(items[index]);
})

app.put('/items/activate/:index', (req,res) =>{

	let index = parseInt(req.params.index);
	items[index].isActive = true;

	res.send(items[index]);
})

// port
app.listen(port, () => console.log(`Server is running at port ${port}`))